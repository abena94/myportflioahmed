import React from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import myImage from "../resources/me.PNG";
import styles from "./About.module.css";

function About() {
  return (
    <div className={styles.aboutsection}>
      <Container maxWidth="md" className="about-container">
        <Box sx={{ height: "40vh" }}>
          {" "}
          <Typography
            variant="h4"
            ml={0}
            pt={20}
            color="#64ffda"
            display="inline"
          >
            01.
          </Typography>
          <Typography
            variant="h4"
            pl={0}
            pt={2}
            color="#ccd6f6"
            display="inline"
          >
            À propos de moi
          </Typography>
          <hr />
          <Box sx={{ display: "block" }}>
            <Box>
              <Typography color="#A8B2D1" className="aboutText">
                Je suis Ahmed Ben Ayed et je suis titulaire d’une attestation de
                réussite au concours national d’entrée aux écoles d’ingénieurs
                en 2015. J’ai également un Baccalauréat en génie mécatronique en
                2019. Actuellement, je suis étudiant en programme d’études
                collégiales en programmation informatique au collège la cité. En
                ce qui concerne mon expérience professionnelle, j’ai fais un
                stage en étude, conception et développement d’un contrôleur bas
                niveau pour un manipulateur robot. Mon soif de connaissance me
                donne toujours l’envie de relever de nouveaux défis. Durant mon
                parcours académique et professionnel, j’ai essayé d’affiner mes
                connaissances dans le domaine de l’informatique. En ce qui
                concerne mes qualités professionnelles, je suis rigoureux,
                résistant au stress et j’ai une aisance relationnelle. Quant à
                mes centres d’intérêts et de loisirs, je suis passionné par les
                sports d’hiver parce que c’est le moyen idéal pour forger mon
                potentiel de concentration et d’attention.
              </Typography>
            </Box>
            <Box sx={{ mx: "auto", width: 200,mt:2,pb:2 }}>
              <Card sx={{ maxWidth: 200 }} className="aboutImage">
                <CardActionArea>
                  <CardMedia component="img" image={myImage} alt="me" />
                </CardActionArea>
              </Card>
            </Box>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default About;
