import { Typography } from "@material-ui/core";
import React from "react";

import styles from "./Project1Desc.module.css";
function ProjectDesc(props) {
  return (
    <div className={styles.DescContainer}>
      
        {props.descData.map((item)=>(
           <Typography>{item.description}</Typography>
        ))}
      
    </div>
  );
}

export default ProjectDesc;
