import React from 'react';
import Typography from '@mui/material/Typography';
function CardTitle(props) {
    return (
        
            <Typography gutterBottom variant="h5" component="div">
             {props.descTitle.map((item)=>(
           <Typography>{item.Title}</Typography>
        ))}
           </Typography>
    
    );
}

export default CardTitle;