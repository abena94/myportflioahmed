import React from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import SchoolIcon from "@mui/icons-material/School";
import ListItemIcon from "@mui/material/ListItemIcon";
import WorkIcon from '@mui/icons-material/Work';
import styles from './Work.module.css'
const Work = () => {
  return (
    <div className={styles.worksection} >
      <Container maxWidth="md" className="work-container">
        <Box sx={{ height: "50vh" }} >
          <Typography
            variant="h4"
            ml={0}
            pt={20}
            color="#64ffda"
            display="inline"
          >
            02.
          </Typography>
          <Typography
            variant="h4"
            pl={0}
            pt={2}
            color="#ccd6f6"
            display="inline"
          >
            Formations et  Expériences
          </Typography>
          <hr />
          <Box >
            <List>
              <ListItem>
                <ListItemIcon>
                  <SchoolIcon style={{ fill: "#ccd6f6" }} />
                </ListItemIcon>

                <Typography color="#A8B2D1">
                  September 2020 - present <br />
                  Diplôme d’études collégiales en programmation informatique,
                  collège La cite
                </Typography>
              </ListItem>

              <ListItem>
                <ListItemIcon>
                  <SchoolIcon style={{ fill: "#ccd6f6" }} />
                </ListItemIcon>

                <Typography color="#A8B2D1">
                  Septembre 2019 <br />
                  Baccalauréat en génie mécatronique, Université libre de
                  Tunis-Tunisie <br />
                  Mention Bien
                </Typography>
              </ListItem>

              <ListItem>
                <ListItemIcon>
                  <WorkIcon style={{ fill: "#ccd6f6" }} />
                </ListItemIcon>

                <Typography color="#A8B2D1">
                Janvier 2019-Septembre2019<br/>
                Stage de fin d’études <br/>
Étude, conception et développement d’un contrôleur bas niveau pour un manipulateur robotique<br/>
Sensing and machine vision for automation and robotic intelligence-research laboratory
Université d’Ottawa
                </Typography>
              </ListItem>

              <ListItem>
                <ListItemIcon>
                  <SchoolIcon style={{ fill: "#ccd6f6" }} />
                </ListItemIcon>

                <Typography color="#A8B2D1">
                Septembre 2015<br/>
Attestation de réussite au concours National d’entrée aux écoles d’ingénieurs <br/>
Institut préparatoire aux écoles d’ingénieurs de Nabeul-Tunisie<br/>
Section Mathématiques et physiques
                </Typography>
              </ListItem>
            </List>
          </Box>
        </Box>
      </Container>
    </div>
  );
};

export default Work;
