import React from "react";
import MyResume from '../resources/resumeahmed.pdf'
import "./Sidebar.css";
import { Gitlab,Linkedin,Application } from "mdi-material-ui";

export const Sidebar = () => {
   
    return (
      <nav className="sidebar">
        
        <ul >
        <span className="vertical-line"></span>
          <li><a href="https://gitlab.com/abena94" className="link"><Gitlab/></a></li>
          <li><a href="https://www.linkedin.com/in/ahmed-ben-ayed-3283b8141/" className="link"><Linkedin /></a></li>
          <li><a href={MyResume} download className="link"><Application/></a></li>
          <span className="vertical-line"></span>
          
        </ul>
      </nav>
    );
  }
;