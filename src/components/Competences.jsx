import React from "react";
import Box from "@mui/material/Box";
import {Container,AppBar} from "@mui/material";
import { styled } from '@mui/material/styles';
import { Typography } from "@mui/material";
import styles from "./Competences.module.css";
import Tab from '@mui/material/Tab';
import {TabContext,TabList,TabPanel} from '@mui/lab';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';

import { createTheme } from '@material-ui/core/styles';

const theme = createTheme({
  
    palette: {
      primary: {
        main: '#1a237e',
      },
      secondary: {
        main: '#a7ffeb',
      },
    },
  
});
const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

function Competences() {
    const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
    return (
<div className={styles.compsection}>
      <Container maxWidth="md" className={styles.compcontainer }>
        <Box sx={{ height: "60vh" }} >
          {" "}
          <Typography
            variant="h4"
            ml={0}
            mt={10}
            pt={20}
            color="#64ffda"
            display="inline"
          >
            03.
          </Typography>
          <Typography
            variant="h4"
            pl={0}
            pt={2}
            color="#ccd6f6"
            display="inline"
          >
            Mes Compétences
          </Typography>
          <hr />
          <Box sx={{ height:"80vh" }} mb={10}>
          <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <AppBar position="static"  color={theme.primary} >
          <TabList onChange={handleChange} variant="scrollable"
  scrollButtons="auto"
  aria-label="scrollable auto tabs example"
 
  textColor="inherit"
  indicatorColor="primary"
  >
      
            <Tab label="Langages de programmation" value="1" />
            <Tab label="Développement Web/Mobile" value="2" />
            <Tab label="Développement Application de Bureau" value="3" />
            <Tab label="Systèmes d’exploitation" value="4" />
            <Tab label="Outils de développement" value="5" />
            <Tab label="Base de données" value="6" />
          </TabList>
          </AppBar>
        </Box>
        <TabPanel value="1">  <Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>Java</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Javascript</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Python</Item>
        </Grid>
        </Grid>
        
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>C#</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>C/C++</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Matlab</Item>
        </Grid>
        </Grid>
      </Grid></TabPanel>
        <TabPanel value="2"><Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>HTML</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>CSS</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Nodejs</Item>
        </Grid>
        </Grid>
        
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>React.js</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Redux</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Android</Item>
        </Grid>
        </Grid>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>Swift</Item>
        </Grid>
        </Grid>
      </Grid></TabPanel>
      <TabPanel value="3"><Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>WINFORM</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Composants WPF</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Netbeans</Item>
        </Grid>
        </Grid>
        
        
      </Grid></TabPanel>
        <TabPanel value="4"><Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={6}>
          <Item>Windows</Item>
        </Grid>
        <Grid item xs={6}>
          <Item>GNU/Linux : Shell</Item>
        </Grid>
        
        </Grid></Grid></TabPanel>
        <TabPanel value="5"><Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>IntelliJ Idea</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Visual Studio Code</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Visual Studio 2019</Item>
        </Grid>

        <Grid item xs={4}>
          <Item>Eclipse</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Git</Item>
        </Grid>
        
        </Grid></Grid></TabPanel>

        <TabPanel value="6"><Grid container spacing={1}>
        <Grid container item spacing={3}>
        <Grid item xs={4}>
          <Item>MySQL</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>SQL Server</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>MongoDB</Item>
        </Grid>

        <Grid item xs={4}>
          <Item>MariaDB</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>FireBase</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Oracle</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>SQLite</Item>
        </Grid>
        
        </Grid></Grid></TabPanel>
      </TabContext>
          </Box>
          </Box>
          </Container>
          </div>
    )
}
export default Competences;