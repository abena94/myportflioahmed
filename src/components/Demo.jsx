import React from 'react';
import ReactPlayer from 'react-player';
import styles from './Demo.module.css'
function Demo(props) {
    return (
        <div className={styles.videoContainer}>
            {props.demourl.map((item)=>
             <ReactPlayer  url={item.url}
                height='500px'
                width='500px'
                controls='true'
            />)}
        </div>
    );
}

export default Demo;