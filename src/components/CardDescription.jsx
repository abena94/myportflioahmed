import React from 'react';
import Typography from '@mui/material/Typography';

function CardDescription(props) {
    return (
        <div>
            {props.cardTech.map((item)=>(
           <Typography variant="body2" color="text.secondary">{item.contenue}</Typography>
        ))}
       </div>
    );
}

export default CardDescription;