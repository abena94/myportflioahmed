import React from "react";
import {
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
  makeStyles,

  useTheme,
  useMediaQuery
} from "@material-ui/core";
import Link from '@mui/material/Link';
import DrawerComponent from "./Drawer";

const useStyles = makeStyles((theme) => ({
  navlinks: {
    marginLeft: theme.spacing(10),
    display: "flex",
  },
 logo: {
    flexGrow: "1",
    cursor: "pointer",
  },
  link: {
    textDecoration: "none",
    color: "white",
    fontSize: "20px",
    paddingLeft: theme.spacing(5),
    "&:hover": {
      color: "#8892B0",
      borderBottom: "1px solid white",
    },
  },
}));

function MenuNav(props) {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  return (
    <AppBar position="static" mb={10}>
      <CssBaseline />
      <Toolbar>
        <Typography variant="h4" className={classes.logo}>
          AHMED
        </Typography>
        {isMobile ? (
          <DrawerComponent />
        ) : (
          <div className={classes.navlinks}>
           <Link href='/' className={classes.link}>Accueil</Link>
            <Link href='/Webprojects' className={classes.link} >WebProjects</Link>
            <Link href='/Otherprojects' className={classes.link} >OtherProjects</Link>
            <Link href='/Contactme' className={classes.link} >Contactme</Link>
          </div>
           )}
      </Toolbar>
    </AppBar>
  );
}
export default MenuNav;