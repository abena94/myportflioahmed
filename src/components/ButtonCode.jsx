import React from 'react';
import Button from '@mui/material/Button';

function ButtonCode(props) {
    return (
        <div> {props.links.map((item)=>
            <Button size="small" href={item.l}>Voir code</Button>
            )

    }</div>
       
        
    );
}

export default ButtonCode;