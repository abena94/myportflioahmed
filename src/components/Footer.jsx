import React from "react";
import styles from '../components/Footer.module.css'
import Box from '@mui/material/Box';
import { Typography } from "@mui/material";
const Footer = () => {
    return ( <div className={styles.footerWrapper}>
        <Box display="flex"
  justifyContent="center"
  alignItems="center"
  sx={{
    width: "100%",
    height: 100,
    m:0 ,
    backgroundColor: "#0A192F",
    '&:hover': {
      backgroundColor: '#233554',
      opacity: [0.9, 0.8, 0.7],
    },
    
  }}
> <Typography variant="h5" color="#E8E8E8"> &copy; Copyright AHMED BEN AYED </Typography></Box>
    </div> );
}
 
export default Footer;