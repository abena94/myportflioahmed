import Images from './Images.jsx'
import Demo from './Demo.jsx'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import styles from './CardProject.module.css'
import Button from '@mui/material/Button';
import { useState } from 'react';
import ProjectDesc from './ProjectDesc.jsx';
import CardTitle from './CardTitle.jsx';
import CardDescription from './CardDescription.jsx';
import ButtonCode from './ButtonCode.jsx';
import { Container } from '@mui/material';


function CardProject (props){
    const [pageCourante, setPageCourante] = useState('projectdesc');

    const changePage = (page) => {
        return () => {
            setPageCourante(page);
        }
    }
    return(
        <div className={styles.containerClass}>
            <Container maxWidth="md">
            <Card sx={{ maxWidth:"800px" }}>
        {pageCourante === 'projectdesc' &&
           <ProjectDesc descData={props.descData}/>
       }

       {pageCourante === 'images' &&
           <Images itemData={props.itemData}  />
       }
       
       {pageCourante === 'demo' &&
           <Demo demourl={props.demourl}/>
       }
       
        <CardContent  align="center">
          <CardTitle descTitle={props.descTitle}/>
         <CardDescription cardTech={props.cardTech} />
        </CardContent>
        <CardActions  style={{justifyContent: 'center'}}>
          <ButtonCode links={props.links} />
          <Button size="small" onClick={changePage('projectdesc')}>Voir Description</Button>
          <Button size="small" onClick={changePage('images')}>Voir Photos</Button>
          <Button size="small" onClick={changePage('demo')}>Voir démonstration vidéo </Button>
        </CardActions>
      </Card></Container></div>
        

    )
}
export default CardProject;