import React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import ImageZoom from "react-image-zooom";

export default function Images(props) {
  return (
    <ImageList sx={{ width: 800, height: 400 }}>
      {props.itemData.map((item) => (
        <ImageListItem key={item.img}>
          
          <ImageZoom
            src={`${item.img}?w=248&fit=crop&auto=format`}
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
            zoom="300"
            width={390}
            height={250}
          />
          <ImageListItemBar
            title={item.title}
            
            position="below"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}


