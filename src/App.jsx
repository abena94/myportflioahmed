import { Routes, Route } from "react-router-dom";
import './App.css';
import { HelmetProvider } from 'react-helmet-async';
import Home from './pages/Home';
import WebProjects from './pages/WebProjects';
import OtherProjects from './pages/OtherProjects';
import Contactme from "./pages/Contactme";
import Header from './components/Header';
import Footer from './components/Footer'

function App() {

  return (
    <>
        <Header />
        <HelmetProvider>
        <Routes>
        <Route path="/" caseSensitive={false} element={<Home />} />
        <Route
          path="/Webprojects"
          caseSensitive={false}
          element={<WebProjects />}
        ></Route>
        <Route
          path="/Otherprojects"
          caseSensitive={false}
          element={<OtherProjects />}
        ></Route>
      
      <Route
          path="/Contactme"
          caseSensitive={false}
          element={<Contactme />}
        ></Route>
      </Routes>
      </HelmetProvider>
       <Footer />
    </>
  );
}

export default App;
