import img1 from "../resources/projet1-1.PNG";
import img2 from "../resources/projet1-2.PNG";
import img3 from "../resources/projet1-3.PNG";
import img4 from "../resources/projet1-4.PNG";
import img5 from "../resources/projet1-5.PNG";
import img6 from "../resources/project1-6.PNG";
import img7 from "../resources/projet1-7.PNG";
import img8  from "../resources/projet1-8.PNG";
import img11 from '../resources/project2-1.PNG'
import img12 from '../resources/project2-2.PNG'
import img13 from '../resources/project2-3.PNG'
import img14 from '../resources/project2-4.PNG'
import img15 from '../resources/project2-5.PNG'
import img16 from '../resources/project2-6.PNG'
import img17 from '../resources/project2-7.PNG'
import img18 from '../resources/project2-8.PNG'
import img21 from '../resources/projet3-1.PNG'
import img22 from '../resources/project3-2.PNG' 
import img23 from '../resources/projet3-3.PNG' 
import img24 from '../resources/projet3-4.PNG' 
import img25 from '../resources/projet3-5.PNG' 
import img26 from '../resources/projet3-6.PNG' 
import img31 from '../resources/1.png';
import img32 from '../resources/2.png';
import img33 from '../resources/3.png';  
export const itemData1 = [
    {
      img: img1,
      title: "Middelware access management",
      
    },
    {
      img: img2,
      title: "Home interface",
      
    },
    {
      img: img3,
      title: "menu Interface",
      
    },
    {
      img: img4,
      title: "Tables",
      
    },
    {
      img: img5,
      title: "page commande",
      
    },
    {
      img: img6,
      title: "Code routes produit avec l'integration de multer",
      
    },
    {
      img: img7,
      title: "Facture",
      
    },
    {
      img: img8,
      title: "Page creer un compte",
      
    },
  ];

  export const itemData2 = [
    {
      img: img11,
      title: 'tables donnees',
     
    },
    {
      img: img12,
      title: 'test route signup',
      
    },
    {
      img: img13,
      title: 'test route signin',
      
    },
    {
      img: img14,
      title: 'validation des comptes',
     
    },
    {
      img: img15,
      title: 'test route creer compte admin',
      
    },
    {
      img: img16,
      title: 'test route se deconnecter',
     
    },
    {
      img: img17,
      title: 'voir tous les utilisateurs a partie dun compte admin',
      
    },
    {
      img: img18,
      title: 'test gestion d accees',
      
    },
  
  ];

  export const itemData3 = [
    {
      img: img21,
      title: "Fetch api externe film",
      
    },
    {
      img: img22,
      title: "Nav Bar",
      
    },
    {
      img: img23,
      title: "movie component ",
      
    },
    {
      img: img24,
      title: "recherche component",
      
    },
    {
      img: img25,
      title: "interface home",
      
    },
    {
      img: img26,
      title: "interface recherche",
      
    },
 
  ];

  export const itemData4 = [
    {
      img: img31,
      title: "Interface stagiaire",
      
    },
    {
      img: img32,
      title: "Interface Etudiant",
      
    },
    {
      img: img33,
      title: "Interface pour consulter des stagiaire par programme",
      
    },
  
 
  ];

  export const descData1 = [
      {
      description:"Ce Projet est un restaurant contient six interfaces : page d'acceuil ,page de création d'un compte,page pour se connecter, page de menu,page de commade ,page pour voir la facture .Le front end est fait avec html bootstrap et javascript. Les requêtes HTTP sont faite par axios ,le stockage des mots de passe est fait dans Local Storage.Le back end est fait en nodejs avec la librairie Express .La modélisation des modèles est faite en mongoDb avec mongoose comme ORM"
      
      }

  ];

  export const descData2 = [
    {
    description:"Ce Projet est la partie back end de l'authentification.Il est fait en nodejs avec Express comme librairie.Le modèle de base de donnée est en mongoDb .On a utilisé jason web token pour l'authentification.Il contient un middelware pour la gestion d'accès des utilisateurs",
    
    }

];

export const descData3 = [
    {
    description:"Ce projet contient seulement une interface home avec un formulaire de recherche.La liste des données est pris à partir d'un API externe et la requête est faite par FETCH.c'est un mini projet pour pratiquer les concepts de base de reactjs.les styles sont faite en material-UI ReactJs",
    
    }

];

export const descData4 = [
  {
  description:"Ce projet est fait en C# avec les composant wpf.Il contient trois interface.La première pour gérer les étudiants la deuxième pour ajouter ou supprimer des stagiaires et la troisième pour gérer les stagiaires On a utilisé SQL server pour créer nos tables.",
  
  }

];


export const descTitle1 = [
    {
    Title:"Restaurant"
    
    }

];

export const descTitle2 = [
    {
    Title:"JWT authentification backend"
    
    }

];

export const descTitle3 = [
    {
    Title:"Mini Movie app"
    
    }

];

export const descTitle4 = [
  {
  Title:"Systeme de Gestion de stagiaire"
  
  }

];
export const cardTech1 = [
    {
    contenue:"Javascript(ES6),bootstrap,axios,Express,nodejs,mongoDB,mongoose,multer,JWT"
    
    }

];

export const cardTech2 = [
    {
    contenue:"nodejs,Express,mongoDb,mongoose,jwt"
    
    }

];

export const cardTech3 = [
    {
    contenue:"reactJs,material-UI,fetch API"
    
    }

];

export const cardTech4 = [
  {
  contenue:"WPF,C#,SQL server"
  
  }

];
export const link1 = [
    {
    l:'https://gitlab.com/abena94/restaurant'
    
    }

];
export const link2 = [
    {
    l:'https://gitlab.com/abena94/user-authentification-nodejs-express-mongodb'
    
    }

];
export const link3 = [
    {
    l:'https://gitlab.com/abena94/user-authentification-nodejs-express-mongodb'
    
    }

];
export const link4 = [
  {
  l:'https://gitlab.com/abena94/user-authentification-nodejs-express-mongodb'
  
  }

];

export const demourl1 = [
    {
    url:'restaurant.mkv'
    
    }

];

export const demourl2 = [
    {
    url:'authentificationbackend.mp4'
    
    }

];

export const demourl3 = [
    {
    url:'minimovieapp.mp4'
    
    }

];

export const demourl4 = [
  {
  url:'wpf.mp4'
  
  }

];