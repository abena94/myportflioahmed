import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Box, TextField, Button, Typography } from "@material-ui/core";
import { Helmet } from "react-helmet-async";
import Container from "@material-ui/core/Container";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import styles from "./Contactme.module.css";

const FORM_ENDPOINT ="https://public.herotofu.com/v1/6eda3690-84be-11ec-9bf2-1331efd4d6b3";
const validationSchema = yup.object().shape({
  fullName: yup
    .string()
    .min(3, "le nom doit etre minimum 3 caractere")
    .max(50, "le nom doit etre pas plus de  50 caractere")
    .matches(/^[A-Za-z\s]+$/, "juste des caracteres sont permis")
    .required("svp entrez votre nom"),
  email: yup
    .string()
    .min(5)
    .max(50)
    .email("ceci n'est pas email svp entrez un email valide")
    .required("svp entrez votre nom"),
  phoneNumber: yup
    .string()
    .min(10, "le numero de telephone doit etre un minimum de 10 numeros")
    .max(20)
    .matches(
      /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
      "le numeros doit etre des nombres"
    ),
  message: yup
    .string()
    .min(5, "Ecrire Votre Message ")
    .max(500)
    .matches(/^[^<>]+$/, "N'inclure pas des symbole speciaux")
    .required("N'oubliez pas d'emntrer votre message ;)"),
});

function Contactme() {
  const [submitted, setSubmitted] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const OnSubmit = () => {
    setTimeout(() => {
      setSubmitted(true);
    }, 100);
  };

  if (submitted) {
    return (
      <>
        <div className={styles.submitdiv}>
          <Container maxWidth="sm">
            <Box sx={{ height: "40vh" }} className={styles.submitcontainer}>
              <Typography
                variant="h1"
                color="inherit"
                mr={8}
                className="submiText"
              >
                Thank you we'll be in touch soon
              </Typography>
            </Box>
          </Container>
        </div>
      </>
    );
  }

  return (
    <>
      <Helmet>
        <title>Page de contact</title>
        <meta name="description" content="Page pour me contacter" />
      </Helmet>
      <div className={styles.contactsection}>
        <Container maxWidth="md" className="work-container">
          <Box className="main">
            <h1 className="main-title">Contact Form</h1>
            <form
              
              action={FORM_ENDPOINT} 
              method="POST"
              target="_blank"
            >
              <TextField
                {...register("fullName")}
                
                fullWidth
                color="primary"
                name="fullName"
                label="Nom"
                variant="outlined"
                margin="normal"
                error={errors.fullName}
              />
              {errors.fullName && (
                <p className="invalidInput">{errors.fullName.message}</p>
              )}

              <TextField
                {...register("email")}
                fullWidth
                color="primary"
                name="email"
                label="Email"
                variant="outlined"
                margin="normal"
                error={errors.email}
              />
              {errors.email && (
                <p className="invalidInput">{errors.email.message}</p>
              )}

              <TextField
                {...register("phoneNumber")}
                fullWidth
                color="primary"
                name="phoneNumber"
                label="Numero de cellulaire"
                variant="outlined"
                margin="normal"
                error={errors.phoneNumber}
              />
              {errors.phoneNumber && (
                <p className="invalidInput">{errors.phoneNumber.message}</p>
              )}

              <TextField
                {...register("message")}
                color="primary"
                multiline
                rows={5}
                fullWidth
                name="message"
                label="Message"
                variant="outlined"
                margin="normal"
                error={errors.message}
              />
              {errors.message && (
                <p className="invalidInput">{errors.message.message}</p>
              )}

              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                onSubmit={handleSubmit(OnSubmit)}
              >
                Envoyer
              </Button>
            </form>
          </Box>
        </Container>
      </div>
    </>
  );
}

export default Contactme;
