import React from "react";

import CardProject from "../components/CardProject";

import {demourl1,demourl2,demourl3,link1,link2,link3, descTitle1,descTitle2,descTitle3,descData1, descData2, descData3,itemData1, itemData2,itemData3, cardTech1, cardTech2,cardTech3 } from "../resources/data";
import styles from './WebProjects.module.css'
import { Sidebar } from "../components/Sidebar";
import { Helmet } from 'react-helmet-async';

function WebProjects(props) {
  return (
    <div className={styles.webPageContainer}> 
    <Helmet>
            <title>
                Page des projets web 
            </title>
            <meta name="description" content="une page pour expliquer des projets de web" />
        </Helmet>
       <Sidebar />
        <CardProject itemData={itemData1} descData={descData1} descTitle={descTitle1} cardTech={cardTech1} links={link1} demourl={demourl1}/>
        <CardProject itemData={itemData2} descData={descData2} descTitle={descTitle2} cardTech={cardTech2} links={link2} demourl={demourl2}/>
        <CardProject itemData={itemData3} descData={descData3} descTitle={descTitle3} cardTech={cardTech3} links={link3} demourl={demourl3}/>
         
      
    </div>
  );
}

export default WebProjects;
