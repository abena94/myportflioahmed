import React from 'react';
import { Sidebar } from "../components/Sidebar";
import CardProject from '../components/CardProject';
import { itemData4,descData4,descTitle4,cardTech4,link4,demourl4 } from '../resources/data';

import { Helmet } from 'react-helmet-async';
import styles from './OtherProjects.module.css'

function OtherProjects(props) {
    return (
        <div className={styles.otherPageContainer}>
            <Helmet>
            <title>
                Page de projets application de bureau 
            </title>
            <meta name="description" content="une page pour expliquer des projets d'applications de bureau" />
        </Helmet>
            <Sidebar />
            <CardProject itemData={itemData4} descData={descData4} descTitle={descTitle4} cardTech={cardTech4} links={link4} demourl={demourl4}/>
         
            

        </div>
    );
}

export default OtherProjects;