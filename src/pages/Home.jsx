import React from 'react';
import About from '../components/About';
import Work from '../components/Work';
import { Sidebar } from '../components/Sidebar';
import { Helmet } from 'react-helmet-async';
import Competences from '../components/Competences';
import styles from "./Home.module.css"


function Home(props) {
    return (
        <div className={styles.homeContainer}>
            <Helmet>
            <title>
                Page d'accueil 
            </title>
            <meta name="description" content="Page d'accueil de mon portflio" />
        </Helmet>
            <Sidebar />
            <About />
            <Work />
            <Competences />            
           
        </div>
    );
}

export default Home;